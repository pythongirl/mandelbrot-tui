#include <stdbool.h>
#include <curses.h>
#include <mpfr.h>

const mpfr_prec_t bits_of_precision = 64;
const int max_iterations = 500;

int main(int argc, char **argv){
	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();

	int max_x, max_y;
	getmaxyx(stdscr, max_y, max_x);
	
	mpfr_t internal_x, internal_y, offset_x, offset_y, zoom_level, temp, scale, z_x, z_y, temp2, temp3;
	mpfr_inits2(bits_of_precision, internal_x, internal_y, offset_x, offset_y, zoom_level, temp, scale, z_x, z_y, temp2, temp3, (mpfr_ptr) NULL);
	mpfr_set_zero(offset_x, 1);
	mpfr_set_zero(offset_y, 1);
	mpfr_set_zero(zoom_level, 1);

	char display_buffer[30];

	int ch = 0;
	do {
		// Change zoom level
		if(ch == 's') mpfr_add_si(zoom_level, zoom_level, 1, MPFR_RNDN);
		if(ch == 'a') mpfr_sub_si(zoom_level, zoom_level, 1, MPFR_RNDN);

		// Calculate scale
		mpfr_neg(temp, zoom_level, MPFR_RNDN);
		mpfr_ui_pow(scale, 2, temp, MPFR_RNDN);

		// Calculate offset change
		mpfr_mul_d(temp, scale, 0.1, MPFR_RNDN); 

		// vi-like controls (with diagonals)
		if(ch == 'h' || ch == 'y' || ch == 'b') mpfr_add(offset_x, offset_x, temp, MPFR_RNDN);
		if(ch == 'l' || ch == 'u' || ch == 'n') mpfr_sub(offset_x, offset_x, temp, MPFR_RNDN);
		if(ch == 'j' || ch == 'b' || ch == 'n') mpfr_sub(offset_y, offset_y, temp, MPFR_RNDN);
		if(ch == 'k' || ch == 'y' || ch == 'u') mpfr_add(offset_y, offset_y, temp, MPFR_RNDN);

		for(int window_y = 0; window_y < max_y; window_y++){
			move(window_y, 0);
			for(int window_x = 0; window_x < max_x; window_x++){
				if(window_x > max_x - 32 && window_y > max_y - 5) break;
				
				// The next 12 LOC are a one-liner in GLSL
				mpfr_div_d(temp, scale, 2.0, MPFR_RNDN);

				mpfr_set_si(internal_x, window_x, MPFR_RNDN);
				mpfr_div_d(internal_x, internal_x, 2, MPFR_RNDN); // Assume each charachter has a 2:1 aspect ratio
				mpfr_div_si(internal_x, internal_x, max_y, MPFR_RNDN);
				mpfr_mul(internal_x, internal_x, scale, MPFR_RNDN);
				mpfr_sub(internal_x, internal_x, temp, MPFR_RNDN);
				mpfr_sub(internal_x, internal_x, offset_x, MPFR_RNDN);

				mpfr_set_si(internal_y, window_y, MPFR_RNDN);
				mpfr_si_sub(internal_y, max_y, internal_y, MPFR_RNDN);
				mpfr_div_si(internal_y, internal_y, max_y, MPFR_RNDN);
				mpfr_mul(internal_y, internal_y, scale, MPFR_RNDN);
				mpfr_sub(internal_y, internal_y, temp, MPFR_RNDN);
				mpfr_add(internal_y, internal_y, offset_y, MPFR_RNDN);
				
				int iterations;
				mpfr_set_zero(z_x, 1);
				mpfr_set_zero(z_y, 1);
				for(iterations = 0; iterations < max_iterations; iterations++){
					// Bailout detection
					mpfr_sqr(temp2, z_x, MPFR_RNDN);
					mpfr_sqr(temp3, z_y, MPFR_RNDN);
					mpfr_add(temp, temp2, temp3, MPFR_RNDN);
					if(mpfr_cmp_d(temp, 4.0) > 0){
						break;
					}

					mpfr_sub(temp, temp2, temp3, MPFR_RNDN);
					mpfr_add(temp, temp, internal_x, MPFR_RNDN);

					mpfr_mul(z_y, z_y, z_x, MPFR_RNDN);
					mpfr_mul_d(z_y, z_y, 2.0, MPFR_RNDN);
					mpfr_add(z_y, z_y, internal_y, MPFR_RNDN);

					mpfr_set(z_x, temp, MPFR_RNDN);
				}

				if(iterations == max_iterations){
					addch('#');
				} else {
					addch(' ');
				}
			}
		}

		mpfr_snprintf(display_buffer, 30, "X: %26.20RNe", offset_x);
		mvprintw(max_y - 3, max_x - 30, display_buffer);
	
		mpfr_snprintf(display_buffer, 30, "Y: %26.20RNe", offset_y);
		mvprintw(max_y - 2, max_x - 30, display_buffer);

		mpfr_snprintf(display_buffer, 30, "S: %26RNe", zoom_level);
		mvprintw(max_y - 1, max_x - 30, display_buffer);

		refresh();
	} while((ch = getch()) != 'q');

	endwin();
	mpfr_clears(internal_x, internal_y, offset_x, offset_y, zoom_level, temp, scale, z_x, z_y, temp2, temp3, (mpfr_ptr) NULL);

	return 0;
}
